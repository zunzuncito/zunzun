#!/usr/bin/env node

import { URL } from 'url';
const __dirname = new URL('.', import.meta.url).pathname;

import { execSync } from 'child_process'

function command(cmd, cwd, stdio_inherit) {
  try {
    console.debug(`\x1b[1m\x1b[44m> Execute command: \x1b[0m\x1b[1m ${cmd}`);
    if (!cwd) cwd = process.cwd()
    console.debug(`\x1b[1m\x1b[45m> CWD: \x1b[0m\x1b[1m ${cwd}\x1b[0m`);
    const result = execSync(cmd, {
      cwd: cwd,
      stdio: stdio_inherit ? 'inherit' : undefined
    });
    if (!stdio_inherit) {
      return result.toString();
    } else {
      return undefined;
    }
  } catch (e) {
    throw e;
  }
}

command("sh post-install.sh", __dirname);
