
download_and_extract() {
    local url="$1"
    local dest_dir="."

    mkdir -p "$dest_dir"

    local filename=$(basename "$url")

    echo "Downloading $url..."
    curl -L -o "$dest_dir/$filename" "$url" || { echo "Failed to download $url"; return 1; }

    echo "Extracting $filename to $dest_dir..."
    case "$filename" in
        *.tar.gz | *.tgz)
            tar -xzf "$dest_dir/$filename" -C "$dest_dir" || { echo "Failed to extract $filename"; return 1; }
            ;;
        *)
            echo "Unsupported archive format: $filename"
            return 1
            ;;
    esac

    # Cleanup: Remove the downloaded archive
    echo "Cleaning up..."
    rm -f "$dest_dir/$filename"

    echo "Download and extraction complete!"
    return 0
}


mkdir -p ~/.zunzun ;\
[ ! -f ~/.zunzun/config.yaml ] &&\
  echo "repo_url: https://zunzuncito.org" > ~/.zunzun/config.yaml ;\
rm -rf twigs/lib/* ;\
mkdir -p twigs/lib ;\
cd twigs/lib ;\
download_and_extract "https://zunzuncito.org/nest/lib/flycfg/lib-flycfg%400.0.8.tar.gz" ;\
download_and_extract "https://zunzuncito.org/nest/lib/flymodule/lib-flymodule%400.0.7.tar.gz" ;\
download_and_extract "https://zunzuncito.org/nest/lib/twigs/lib-twigs%400.0.16.tar.gz" ;\
download_and_extract "https://zunzuncito.org/nest/lib/flypto/lib-flypto%400.1.10.tar.gz" ;\
download_and_extract "https://zunzuncito.org/nest/lib/flyutil/lib-flyutil%400.1.17.tar.gz" ;\
download_and_extract "https://zunzuncito.org/nest/lib/srv-mg/lib-srv-mg%400.0.5.tar.gz" ;\
cd ../.. && twigs install --sync-local
cd node_modules && mkdir -p zunzun && cd zunzun ;\
ln -sf ../../twigs/lib/twigs ;\
ln -sf ../../twigs/lib/flyutil ;\
ln -sf ../../twigs/lib/flypto ;\
ln -sf ../../twigs/lib/flycfg ;\
ln -sf ../../twigs/lib/flymodule ;\
ln -sf ../../twigs/lib/srv-mg ;

