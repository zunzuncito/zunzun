
import YAML from 'yaml'

import os from 'os'

import fs from 'fs'
import path from 'path'

import { URL } from 'url';
const __dirname = new URL('.', import.meta.url).pathname;

function cp_recursive(from, to) {
  console.log("COPY DIR", from, to);
  fs.mkdirSync(to);
  const dir_files = fs.readdirSync(from);
  for (const file of dir_files) {
    const file_path = path.resolve(from, file);
    const file_path_dest = path.resolve(to, file);
    if (fs.lstatSync(file_path).isDirectory()) {
      cp_recursive(file_path, file_path_dest);
    } else {
      fs.copyFileSync(file_path, file_path_dest)
    }
  }

}

const create_symlink = (from, to) => {
  try {
    fs.unlinkSync(to)
  } catch (e) {

  }
  fs.symlinkSync(from, to);
}


const cwd = process.cwd();
const twig_json_path = path.resolve(cwd, 'twig.json');
const twig_json = JSON.parse(fs.readFileSync(twig_json_path, 'utf8'));


const config_yaml_path = path.resolve(os.homedir(), '.zunzun/config.yaml');
const config_yaml = YAML.parse(fs.readFileSync(config_yaml_path, 'utf8'));

for (let lib in twig_json.lib) {
  const local_path = path.resolve(config_yaml.local_twigs, "lib", lib);
  const install_path = path.resolve(cwd, "twigs/lib", lib);
  try { fs.rmSync(install_path, { recursive: true }) } catch (e) { }
  fs.symlinkSync(local_path, install_path);
}



const nm_zunzun = path.resolve(__dirname, 'node_modules/zunzun');
const zunzun_libs = path.resolve(__dirname, 'twigs/lib');

try { fs.rmSync(nm_zunzun, { recursive: true }) } catch (e) { }
fs.mkdirSync(nm_zunzun)

const local_libs = fs.readdirSync(zunzun_libs);

for (let ldir of local_libs) {
  let from = path.resolve(zunzun_libs, ldir);
  let to = path.resolve(nm_zunzun, ldir);

  if (fs.lstatSync(from).isSymbolicLink()) {
    from = fs.readlinkSync(from)

    try { fs.rmSync(to, { recursive: true }) } catch (e) { }

    cp_recursive(from, to)

  } else {
    create_symlink(from, to);
  }
}
