# Zunzuncito #
##### [world's smallest bird](https://en.wikipedia.org/wiki/Bee_hummingbird) #####

#### Lightweight application development framework ####

### Installation ###

`npm install zunzun`

### Quick start ###

Run this command in the directory `.zunzun.yaml`:
```
  zunsrv
```

Run a script:
```
  zuncli [script] [args]
```

Package manager `twigs help` or `twigs h`
