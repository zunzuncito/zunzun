
import FlyCfg from 'zunzun/flycfg/index.mjs'
import ServiceManager from 'zunzun/srv-mg/index.mjs'

const CWD = process.cwd();

(async () => {
  const { layers, app_cfg } = await FlyCfg.project(CWD);
  const srv_mg = new ServiceManager(layers, app_cfg);
  await srv_mg.preconf();
  await srv_mg.start();
})()

