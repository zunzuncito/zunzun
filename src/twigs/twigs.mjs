
import fs from 'fs'
import os from 'os'
import path from 'path'

import YAML from 'yaml'

import Twigs from 'zunzun/twigs/index.mjs'

(async () => {
  try {

    if (process.argv.length > 2) {
      const action = process.argv[2];
      const args = process.argv.slice(3, process.argv.length);

      const cwd = process.cwd();
      const twig_json_path = path.resolve(cwd, 'twig.json');
      let twig_json = undefined;
      if (fs.existsSync(twig_json_path)){
        JSON.parse(fs.readFileSync(twig_json_path, 'utf8'));
      }

      const config_yaml_path = path.resolve(os.homedir(), '.zunzun/config.yaml');
      const config_yaml = YAML.parse(fs.readFileSync(config_yaml_path, 'utf8'));
      config_yaml.zunzun_path = path.resolve(os.homedir(), '.zunzun');

      if (action) {

        switch(action) {
          case 'publish':
            await Twigs.Publish.one(cwd, args, config_yaml);
            break;
          case 'install':
            await Twigs.Install.by_command(cwd, args, config_yaml);
            break;
          case 'init':
            await Twigs.Init.by_command(cwd, args, config_yaml);

          case 'clone-local':
            await Twigs.LocalRepos.clone(args, config_yaml);
            break;
          default:
            console.log(`

        twigs [!action] [pkg_type] [pkg_name]

            `);
        }
      }
    } else {
      console.log(`

        twigs [!action] [pkg_type] [pkg_name]

      `);
    }
  } catch (e) {
    console.error(e.stack);
  }
})();
