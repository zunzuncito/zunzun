

import fs from "fs"
import path from "path"

import os from "os"

import YAML from 'yaml'

import FlyCfg from 'zunzun/flycfg/index.mjs'
import FlyModules from 'zunzun/flymodule/index.mjs'
import ObjectUtil from 'zunzun/flyutil/object.mjs'
import PathUtil from 'zunzun/flyutil/path.mjs'
import ServiceManager from 'zunzun/srv-mg/index.mjs'


const CWD = process.cwd();

let data_dir = path.resolve(os.homedir(), ".zunzun");
const zunzun_config_path = path.resolve(data_dir, "config.yaml");
const zunzun_json_path = PathUtil.resolve_by_type('.zunzun', 'yaml');


let zunzun_config = undefined;
if (fs.existsSync(zunzun_config_path)) {
  try {
    zunzun_config = YAML.parse(fs.readFileSync(zunzun_config_path, "utf8"));
  } catch (e) {
    console.error(e.stack);
    zunzun_config = {};
  }
}

(async () => {
  try {
    const script_name = process.argv[2];
    const script_path = path.resolve(data_dir, "cli", script_name);
    const cliconfig_path = path.resolve(script_path, '.cliconfig.yaml');
    let cliconfig = undefined;
    try {
      cliconfig = YAML.parse(fs.readFileSync(cliconfig_path, 'utf8'));
    } catch (e) {
      console.error(e.stack);
    }

    let project_config = undefined;

    if (cliconfig) {
      if (fs.existsSync(zunzun_json_path)) {
        try {
          const { layers, app_cfg } = await FlyCfg.project(CWD);
          project_config = app_cfg;
        } catch (e) {
          console.error(e.stack);
          process.exit();
        }
      }

    }

    if (project_config && Array.isArray(project_config.cli)) {
      for (let cli of project_config.cli) {
        if (cli.name == script_name) {
          cliconfig = ObjectUtil.force_add(cliconfig, cli.config)
        }
      }
    }

    await FlyModules.run_script(script_name, script_path, cliconfig, zunzun_config, project_config);

  } catch (e) {
    console.error(e.stack);
  }
})();
